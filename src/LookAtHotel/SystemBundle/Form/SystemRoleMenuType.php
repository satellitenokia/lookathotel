<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemRoleMenuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'menu',
                'entity',
                array(
                    'label' => 'menu',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'checkbox check-primary'
                    ),
                    'class' => 'SystemBundle:Menu',
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                        $qb = $repository->createQueryBuilder('menu');
                        $qb->add('select', 'm')
                        ->add('from', 'SystemBundle:Menu m')
                        // ->add('where', 'm.menuParent IS NULL');
                        ->orderBy('m.orderItem', 'ASC');

                        return $qb;
                    },
                    'property'    => 'title',
                    'expanded'    => true,
                    'multiple'    => true,
                    'required'    => true,
                    'empty_value' => false,
                    'empty_data'  => false,

                )
            )

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LookAtHotel\SystemBundle\Entity\SystemRole'
        ));
    }

    public function getName()
    {
        return 'opentech_sds_systembundle_systemrole';
    }
}
