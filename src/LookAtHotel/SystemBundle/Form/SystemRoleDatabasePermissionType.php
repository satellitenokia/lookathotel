<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemRoleDatabasePermissionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'entity',
                'entity',
                array(
                    'label' => 'entity',
                    'label_attr' => array(
                        'class' => 'control-label pull-right',
                        'style' => 'padding-left: 20px;',
                    ),
                    'attr' => array(
                        'class' => 'checkbox check-primary',
                    ),
                    'class'       => 'SystemBundle:Entity',
                    'property'    => 'entity',
                    'expanded'    => true,
                    'multiple'    => true,
                    'required'    => true,
                    'empty_value' => false,
                    'empty_data'  => false,

                )
            )

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LookAtHotel\SystemBundle\Entity\SystemRole'
        ));
    }

    public function getName()
    {
        return 'opentech_sds_systembundle_systemrole';
    }
}
