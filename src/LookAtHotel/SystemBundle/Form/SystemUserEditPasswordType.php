<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemUserEditPasswordType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'password',
                'repeated',
                array(
                    'type' => 'password',
                    'invalid_message' => 'no.match.pass',
                    'required' => false,
                    'first_options'  => array(
                        'label' => 'pass',
                        'label_attr' => array(
                            'class' => 'control-label'
                        ),
                        'attr' => array(
                            'class' => 'form-control'
                        ),
                    ),
                    'second_options' => array(
                        'label' => 'repeat.pass',
                        'label_attr' => array(
                            'class' => 'control-label'
                        ),
                        'attr' => array(
                            'class' => 'form-control'
                        ),
                    ),
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'LookAtHotel\SystemBundle\Entity\SystemUser',
            'validation_groups' => array('password')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'opentech_sds_systembundle_systemuser';
    }
}
