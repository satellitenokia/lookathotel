<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemRoleSystemPermissionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'action',
                'entity',
                array(
                    'label' => 'action',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'checkbox check-primary'
                    ),
                    'class'       => 'SystemBundle:Action',
                    'property'    => 'action',
                    'expanded'    => true,
                    'multiple'    => true,
                    'required'    => true,
                    'empty_value' => false,
                    'empty_data'  => false,

                )
            )

        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LookAtHotel\SystemBundle\Entity\SystemRole'
        ));
    }

    public function getName()
    {
        return 'opentech_sds_systembundle_systemrole';
    }
}
