<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MenuChildType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'menuParent',
                'entity',
                array(
                    'label' => 'menu.parent',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'select2 form-control'
                    ),
                    'class' => 'SystemBundle:Menu',
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) {
                        $qb = $repository->createQueryBuilder('menuparent');
                        $qb->add('select', 'm')
                        ->add('from', 'SystemBundle:Menu m')
                        ->add('where', 'm.menuParent IS NULL');
                        // ->setParameter('parent', NULL);

                        return $qb;
                    },
                    'property'    => 'title',
                    'expanded'    => false,
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                    'empty_data'  => false,
                )
            )

            ->add(
                'title',
                'text',
                array(
                    'label' => 'title',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false,
                )
            )

            ->add(
                'path',
                'text',
                array(
                    'label' => 'access.route',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false,
                )
            )

            ->add(
                'orderItem',
                'choice',
                array(
                    'label' => 'order',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'select2 form-control'
                    ),
                    'choices' => array(
                        '1'  => '1' ,
                        '2'  => '2' ,
                        '3'  => '3' ,
                        '4'  => '4' ,
                        '5'  => '5' ,
                        '6'  => '6' ,
                        '7'  => '7' ,
                        '8'  => '8' ,
                        '9'  => '9' ,
                        '10' => '10',
                        '11' => '11',
                        '12' => '12',
                        '13' => '13',
                        '14' => '14',
                        '15' => '15',
                        '16' => '16',
                        '17' => '17',
                        '18' => '18',
                        '19' => '19',
                        '20' => '20',
                    ),
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true,
                )
            )

            ->add(
                'isExternal',
                'choice',
                array(
                    'label' => 'external.link',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'radio radio-primary'
                    ),
                    'choices' => array(
                        '1' => 'yes',
                        '0' => 'no',
                    ),
                    'expanded'    => true,
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                )
            )

            ->add(
                'inNewTab',
                'choice',
                array(
                    'label' => 'open.new.tag',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'radio radio-primary'
                    ),
                    'choices' => array(
                        '1' => 'yes',
                        '0' => 'no',
                    ),
                    'expanded'    => true,
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LookAtHotel\SystemBundle\Entity\Menu'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'opentech_sds_systembundle_menu';
    }
}
