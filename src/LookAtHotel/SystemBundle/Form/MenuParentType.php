<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MenuParentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                'text',
                array(
                    'label' => 'title',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false,
                )
            )

            ->add(
                'icon',
                'choice',
                array(
                    'label' => 'icon',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'select2 form-control'
                    ),
                    'choices' => array(
                        'icon-custom-home'     => 'Home - [icon-custom-home]',
                        'fa fa-th'             => 'Widgets - [fa fa-th]',
                        'fa fa-envelope'       => 'Email - [fa fa-envelope]',
                        'fa fa-flag'           => 'Bandera - [fa fa-flag]',
                        'fa fa-adjust'         => 'Ajuste - [fa fa-adjust]',
                        'fa fa-file-text'      => 'Archivo - [fa fa-file-text]',
                        'icon-custom-ui'       => 'Etiqueta - [icon-custom-ui]',
                        'icon-custom-form'     => 'Formulario - [icon-custom-form]',
                        'icon-custom-portlets' => 'Libreta - [icon-custom-portlets]',
                        'icon-custom-thumb'    => 'Tablas - [icon-custom-thumb]',
                        'icon-custom-map'      => 'Marcador de GPS - [icon-custom-map]',
                        'fa fa-crosshairs'     => 'Localización - [fa fa-crosshairs]',
                        'fa fa-road'           => 'Ruta - [fa fa-road]',
                        'icon-custom-chart'    => 'Gráficos - [icon-custom-chart]',
                        'icon-custom-extra'    => 'Carrito - [icon-custom-extra]',
                        'fa fa-folder-open'    => 'Carpeta - [fa fa-folder-open]',
                        'fa fa-group'          => 'Vendedores - [fa fa-group]',
                        'fa fa-globe'          => 'Mapa - [fa fa-globe]',
                    ),
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true,
                )
            )

            ->add(
                'orderItem',
                'choice',
                array(
                    'label' => 'order',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'select2 form-control'
                    ),
                    'choices' => array(
                        '1'  => '1' ,
                        '2'  => '2' ,
                        '3'  => '3' ,
                        '4'  => '4' ,
                        '5'  => '5' ,
                        '6'  => '6' ,
                        '7'  => '7' ,
                        '8'  => '8' ,
                        '9'  => '9' ,
                        '10' => '10',
                        '11' => '11',
                        '12' => '12',
                        '13' => '13',
                        '14' => '14',
                        '15' => '15',
                        '16' => '16',
                        '17' => '17',
                        '18' => '18',
                        '19' => '19',
                        '20' => '20',
                    ),
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true,
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LookAtHotel\SystemBundle\Entity\Menu'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'opentech_sds_systembundle_menu';
    }
}
