<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemUserType extends AbstractType
{

    private $userRole;

    public function __construct($userRole)
    {
        $this->userRole = $userRole;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userRole = $this->userRole;

        $builder
                ->add(
                        'username', 'text', array(
                    'label'      => 'user',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr'       => array(
                        'class' => 'form-control'
                    ),
                    'required'   => false,
                        )
                )
                ->add(
                        'password', 'repeated', array(
                    'type'            => 'password',
                    'invalid_message' => 'no.match.pass',
                    'required'        => false,
                    'first_options'   => array(
                        'label'      => 'pass',
                        'label_attr' => array(
                            'class' => 'control-label'
                        ),
                        'attr'       => array(
                            'class' => 'form-control'
                        ),
                    ),
                    'second_options'  => array(
                        'label'      => 'repeat.pass',
                        'label_attr' => array(
                            'class' => 'control-label'
                        ),
                        'attr'       => array(
                            'class' => 'form-control'
                        ),
                    ),
                        )
                )
                ->add(
                        'firstname', 'text', array(
                    'label'      => 'name',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr'       => array(
                        'class' => 'form-control'
                    ),
                    'required'   => false,
                        )
                )
                ->add(
                        'lastname', 'text', array(
                    'label'      => 'last.name',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr'       => array(
                        'class' => 'form-control'
                    ),
                    'required'   => false,
                        )
                )
                ->add(
                        'email', 'text', array(
                    'label'      => 'email',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr'       => array(
                        'class' => 'form-control'
                    ),
                    'required'   => false,
                        )
                )
                ->add(
                        'systemRole', 'entity', array(
                    'label'         => 'roles',
                    'label_attr'    => array(
                        'class' => 'control-label'
                    ),
                    'attr'          => array(
                        // 'class' => 'select2 form-control'
                        'class' => 'checkbox check-primary role-selector'
                    ),
                    'class'         => 'SystemBundle:SystemRole',
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $repository) use ($userRole) {

                        $qb = $repository->createQueryBuilder('z_alias');
                        $qb->add('select', 'r')
                        ->add('from', 'SystemBundle:SystemRole r')
                        ->add('where', ' r.role <> :userRole')
                        ->setParameter('userRole', $userRole);

                        return $qb;
                    },
                    'property'    => 'role',
                    'expanded'    => true,
                    'multiple'    => true,
                    'required'    => true,
                    'empty_value' => false,
                    'empty_data'  => false,
                        )
                )
                ->add(
                        'active', 'choice', array(
                    'label'       => 'active',
                    'label_attr'  => array(
                        'class' => 'control-label'
                    ),
                    'attr'        => array(
                        'class' => 'radio radio-primary'
                    ),
                    'choices'     => array(
                        '1' => 'yes',
                        '0' => 'no',
                    ),
                    'expanded'    => true,
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                        )
                )

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'LookAtHotel\SystemBundle\Entity\SystemUser',
            'validation_groups' => array('profile', 'password')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'opentech_sds_systembundle_systemuser';
    }

}
