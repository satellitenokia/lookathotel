<?php

namespace LookAtHotel\SystemBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SystemRoleType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'role',
                'text',
                array(
                    'label' => 'role',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false,
                )
            )

            ->add(
                'label',
                'text',
                array(
                    'label' => 'label',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false,
                )
            )

            ->add(
                'description',
                'text',
                array(
                    'label' => 'description',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'required' => false,
                )
            )

            ->add(
                'level',
                'choice',
                array(
                    'label' => 'role.level',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'select2 form-control'
                    ),
                    'choices' => array(
                        '10' => '10',
                        '9'  => '9' ,
                        '8'  => '8' ,
                        '7'  => '7' ,
                        '6'  => '6' ,
                        '5'  => '5' ,
                        '4'  => '4' ,
                        '3'  => '3' ,
                        '2'  => '2' ,
                        '1'  => '1' ,
                    ),
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true,
                )
            )

            ->add(
                'roleAdmin',
                'choice',
                array(
                    'label' => 'admin.role',
                    'label_attr' => array(
                        'class' => 'control-label'
                    ),
                    'attr' => array(
                        'class' => 'radio radio-primary'
                    ),
                    'choices' => array(
                        '1' => 'yes',
                        '0' => 'no',
                    ),
                    'expanded'    => true,
                    'multiple'    => false,
                    'required'    => true,
                    'empty_value' => false,
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LookAtHotel\SystemBundle\Entity\SystemRole'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'opentech_sds_systembundle_systemrole';
    }
}
