<?php

/*
 * InteractiveLogin
 * Manejo del evento post-login
 *
 * Captura el evento post logueo correcto para permitir
 * realizar trabajos sobre los datos introducidos en el
 * formulario de login
 */

namespace LookAtHotel\SystemBundle\DependencyInjection;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use LookAtHotel\SystemBundle\DependencyInjection\CryptoEncoder;
use LookAtHotel\SystemBundle\Entity\Session;

class InteractiveLogin
{

    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Capturar evento por login exitoso
     *
     * @param InteractiveLoginEvent $event
     *
     * @return null
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        // Instanciar CryptoEncoder
        $cryptoEncoder = new CryptoEncoder();

        // obtener el security context a través del container del servicio
        $user = $this->container->get('security.context');

        // obtener los roles que utiliza el usuario
        $token = $user->getToken()->getUser();

        // Obtener el usuario
        $dbUser = $token->getUsername();

        // Obtener la contraseña y desencriptar
        $dbPassword = $cryptoEncoder->decrypt($token->getPassword(), $token->getSalt());

        // Opciones personalizadas de conexión
        $dbOptions = array(
            'driver'   => $this->container->getParameter('database_driver'),
            'host'     => $this->container->getParameter('database_host'),
            'port'     => $this->container->getParameter('database_port'),
            'dbname'   => $this->container->getParameter('database_name'),
            'user'     => $dbUser,
            'password' => $dbPassword,
            'charset'  => 'UTF8'
        );

        // MENU
        // obtener los roles que utiliza el usuario
        $roles = $token->getRolesObj(); //= $user->getToken()->getRoles();
        // Lista de roles para control de permisos
        $roleList = null;

        $menuList = array();

        // obtener el menu por cada rol
        foreach ($roles as $role)
        {
            // Comprobar que no sea el Rol por defecto
            if ($role->getRole() != 'ROLE_DEFAULT')
            {
                $menuList[] = $role->getMenu();
                $roleList[] = $role->getId();
            }
        }

        $cleanMenu = null;

        // iterar entre los resultados de menu de cada rol
        // usando el mismo id siempre para evitar que existan
        // duplicados a la hora de implementar el menu
        foreach ($menuList as $menu)
        {
            foreach ($menu as $item)
            {
                $cleanMenu[$item->getId()]['id']    = $item->getId();
                $cleanMenu[$item->getId()]['title'] = $item->getTitle();
                $cleanMenu[$item->getId()]['path']  = $item->getPath();
                if ($item->getMenuParent() == null)
                {
                    $cleanMenu[$item->getId()]['parent'] = '0';
                }
                else
                {
                    $cleanMenu[$item->getId()]['parent']     = $item->getMenuParent()->getId();
                }
                $cleanMenu[$item->getId()]['icon']       = $item->getIcon();
                $cleanMenu[$item->getId()]['order']      = $item->getOrderItem();
                $cleanMenu[$item->getId()]['isExternal'] = $item->getIsExternal();
                $cleanMenu[$item->getId()]['inNewTab']   = $item->getInNewTab();
            }
        }

        if ($cleanMenu != null)
        {
            // ordenar el array de menu según order
            uasort($cleanMenu, function ($a, $b) {
                        return strnatcmp($a['order'], $b['order']);
                    }
            );
        }

        // obtener la sesion
        $session = $this->container->get('session');

        // almacenar db en la sesión
        //$session->set('dbOptions', $dbOptions);

        // almacenar menu en la sesión
        $session->set('userMenu', $cleanMenu);

        // almacenar lista de roles en la sesion
        $session->set('roleList', $roleList);

        // Update entity user information
        $em = $this->container->get('doctrine')->getManager();

        // Reset login information
        $entitySystemUser = $user->getToken()->getUser();

        $entitySystemUser->setLoginAttemps(0);
        $entitySystemUser->setLockedUntil(null);
        $entitySystemUser->setLocked(false);
        $entitySystemUser->setLastLogin(new \DateTime());

        try
        {
            $em->persist($entitySystemUser);
            $em->flush();
        }
        catch (\Doctrine\DBAL\DBALException $e)
        {
            die($e->getMessage());
        }


        // Registrar una sesión de usuario
        // Obtener configuración del sistema
        $entitySystemConfiguration = $em->getRepository('SystemBundle:SystemConfiguration')->findOneBy(
                array(), array('createdAt' => 'DESC')
        );

        // Limpiar todas las sesiones
        // 1. Sesiones caducadas
        $query = $em->createQuery("UPDATE SystemBundle:Session se SET se.status = 'I' WHERE se.username = :username AND se.status = :status AND se.validUntil < :now");
        $query->setParameter('username', $entitySystemUser->getUsername());
        $query->setParameter('status', 'A');
        $query->setParameter('now', new \DateTime());

        // Devolver los resultados de la consulta
        $query->getResult();


        // 2. Sesiones del mismo user/máquina sin cerrar
        // Consulta SQL para buscar una sesión activa
        $query = $em->createQuery("UPDATE SystemBundle:Session s SET s.status = 'I' WHERE s.username = :username AND s.status = :status AND s.validUntil >= :now AND s.ipAddress = :ipAddress");
        $query->setParameter('username', $user->getToken()->getUser()->getUsername());
        $query->setParameter('status', 'A');
        $query->setParameter('now', new \DateTime());
        $query->setParameter('ipAddress', $this->getIpAddress());

        // Devolver los resultados de la consulta
        $query->getResult();


        // Comprobar si permite tener múltiples sesiones
        if (!$entitySystemConfiguration->getAllowMultipleSessions())
        {
            // Limpiar todas las sesiones
            // Query
            $query = $em->createQuery("UPDATE SystemBundle:Session se SET se.status = 'I' WHERE se.username = :username AND se.status = :status");
            $query->setParameter('username', $entitySystemUser->getUsername());
            $query->setParameter('status', 'A');

            // Devolver los resultados de la consulta
            $query->getResult();
        }

        // Registrar la sesión
        $validUntil = new \DateTime("+" . $entitySystemConfiguration->getSessionTimeout() . " seconds");

        $userSession = new Session();
        $userSession->setSessionId($session->getId());
        $userSession->setUsername($entitySystemUser->getUsername());
        $userSession->setProcessId(getmypid());
        $userSession->setIpAddress($this->getIpAddress());
        $userSession->setChannel('WEB');
        $userSession->setStatus('A');
        $userSession->setCreatedAt(new \DateTime());
        $userSession->setValidUntil($validUntil);
        $userSession->setUpdatedAt(new \DateTime());

        try
        {
            $em->persist($userSession);
            $em->flush();
        }
        catch (\Doctrine\DBAL\DBALException $e)
        {
            die($e->getMessage());
        }
    }

    // obtener la dirección IP del cliente
    private function getIpAddress()
    {
        $ip = filter_var($this->container->get('request')->getClientIp(), FILTER_VALIDATE_IP);
        return $ip === false ? '0.0.0.0' : $ip;
    }

}