<?php

/*
 * Access Control Layer
 * Control de permisos de usuario
 *
 * Verifica que un usuario tenga los privilegios suficientes para
 * acceder a un action de un controller en particular
 */

namespace LookAtHotel\SystemBundle\DependencyInjection;


class AccessControlLayer
{
    /**
     * Verificar permisos
     *
     * @param string $roles lista de roles del usuario
     *
     * @return boolean concede o no los permisos
     */
    public function checkPermission($roleList, $container)
    {
        /*
         * Cuando se necesita iniciar la aplicación sin datos,
         * descomentar esta sección para acceso directo
         */

        // Obtener los valores de conexión del fichero parameters.yml
        /*$dbOptionValues['driver']   = $container->getParameter('database_driver');
        $dbOptionValues['host']     = $container->getParameter('database_host');
        $dbOptionValues['port']     = $container->getParameter('database_port');
        $dbOptionValues['dbname']   = $container->getParameter('database_name');
        $dbOptionValues['user']     = $container->getParameter('database_user');
        $dbOptionValues['password'] = $container->getParameter('database_password');
        $dbOptionValues['charset']  = "UTF-8";

        // Asignar array construido para almacenar en la sesión
        $dbOptions = $dbOptionValues;

        // obtener la sesion
        $session = $container->get('session');

        // almacenar menu en la sesión
        $session->set('dbOptions', $dbOptions);


        // Update entity user information
        $em = $container->get('doctrine')->getManager();

        // obtener el security context a través del container del servicio
        $user = $container->get('security.context');

        // Obtener configuración del sistema
        $entitySystemConfiguration = $em->getRepository('SystemBundle:SystemConfiguration')->findOneBy(
                array(), array('createdAt' => 'DESC')
        );

        // Consulta SQL para buscar una sesión activa
        $query = $em->createQuery("SELECT s FROM SystemBundle:Session s WHERE s.username = :username AND s.status = :status AND s.validUntil >= :now AND s.ipAddress = :ipAddress");
        $query->setParameter('username', $user->getToken()->getUser()->getUsername());
        $query->setParameter('status', 'A');
        $query->setParameter('now', new \DateTime());
        $query->setParameter('ipAddress', $this->getIpAddress($container));

        // Devolver los resultados de la consulta
        $entitySession = $query->getResult();

        $result            = true;
        $invalidateSession = false;

        if (count($entitySession) <= 0)
        {
            $invalidateSession = true;
        }
        else
        {
            $entitySession = $entitySession['0'];

            $updatedAt = $entitySession->getUpdatedAt();
            $updatedAt->modify("+" . $entitySystemConfiguration->getMaxIdleTime() . " seconds");
            $now       = new \DateTime();

            if ($now > $updatedAt)
            {
                $invalidateSession = true;
            }
            else
            {
                $entitySession->setUpdatedAt(new \DateTime());

                try
                {
                    $em->persist($entitySession);
                    $em->flush();
                }
                catch (\Doctrine\DBAL\DBALException $e)
                {
                    die($e->getMessage());
                }
            }
        }

        // Si la sesión expiró, entonces retornar que no hay permiso para ejecutar
        if ($invalidateSession)
        {
            $session->clear();
            $session->invalidate();
            return array('permiso' => false, 'sesion'  => false, 'type'    => 'session');
        }

        return array('permiso' => true, 'sesion'  => true, 'type'    => 'none');

        /*
         * FIN INICIO SIN DATOS
         */

        // Lista de roles
        $roles = implode(",", $roleList);

        // Obtener el request
        $request = $container->get('request');

        // Obtener el namespace a ejecutar
        $namespace = $request->get('_controller');

        // Separar action del resto del namespace
        $namespace = explode("::", $namespace);

        // Obtener el action
        $action = $namespace['1'];

        // Separar el resto del namespace
        $namespace = explode("\\", $namespace['0']);

        // Obtener bundle y controller
        $bundle     = $namespace['1'];
        $controller = $namespace['3'];

        // Obtener conexión a la base de datos
        $conn = $container->get('database_connection');

        // Consulta SQL para verificar permisos
        $sql = "SELECT COUNT(p.system_role_id) as permiso ";
        $sql .= "FROM bundle b ";
        $sql .= "INNER JOIN controller c ON b.id = c.bundle_id ";
        $sql .= "INNER JOIN action a ON c.id = a.controller_id ";
        $sql .= "INNER JOIN system_permission p ON a.id = p.action_id ";
        $sql .= "WHERE b.bundle = '" . $bundle . "' ";
        $sql .= "AND c.controller = '" . $controller . "' ";
        $sql .= "AND a.action = '" . $action . "' ";
        $sql .= "AND p.system_role_id IN (" . $roles . ") ";

        $result = $conn->query($sql);
        $result = $result->fetchAll();

        if ($result['0']['permiso'] > 0)
        {
            return array('permiso' => true, 'sesion'  => true, 'type'    => 'none');
        }
        else
        {
            return array('permiso' => false, 'sesion'  => true, 'type'    => 'permission');
        }
    }

    // obtener la dirección IP del cliente
    private function getIpAddress($container)
    {
        $ip = filter_var($container->get('request')->getClientIp(), FILTER_VALIDATE_IP);
        return $ip === false ? '0.0.0.0' : $ip;
    }

}