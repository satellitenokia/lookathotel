<?php

/*
 * CryptoEncoder
 * Clase de Encriptación
 *
 * Implementación de métodos para encriptación y desencriptación de passwords
 * a través de un encoder propio.
 *
 * Implementa los métodos de Symfony "PasswordEncoderInterface" para validar
 * las contraseñas.
 */

namespace LookAtHotel\SystemBundle\DependencyInjection;

use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

class CryptoEncoder implements PasswordEncoderInterface
{
	/**
     * Encriptar contraseña
     *
     * @param string $text texto plano de la contraseña
     * @param string $salt semilla de texto para encriptar
     *
     * @return string contraseña encriptada
     */
	protected function encrypt($text, $salt)
	{
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
	}

	/**
     * Desencriptar contraseña
     *
     * @param string $text texto plano de la contraseña
     * @param string $salt semilla de texto para encriptar
     *
     * @return string contraseña desencriptada
     */
	public function decrypt($text, $salt)
	{
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
	}

	/**
     * Codificar contraseña, método de la interfaz PasswordEncoderInterface
     *
     * @param string $text texto plano de la contraseña
     * @param string $salt semilla de texto para encriptar
     *
     * @return string contraseña encriptada
     */
	public function encodePassword($text, $salt)
	{
		return $this->encrypt($text, $salt);
	}

	/**
     * Validar contraseña, método de la interfaz PasswordEncoderInterface
     *
     * @param string $encoded contraseña encriptada
     * @param string $text texto plano de la contraseña
     * @param string $salt semilla de texto para encriptar
     *
     * @return bool contraseña válida o inválida
     */
	public function isPasswordValid($encoded, $text, $salt)
	{
		return $encoded === $this->encodePassword($text, $salt);
	}
}