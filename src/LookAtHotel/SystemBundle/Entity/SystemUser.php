<?php

namespace LookAtHotel\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
//use LookAtHotel\SystemBundle\DependencyInjection\CryptoEncoder;

/**
 * SystemUser
 *
 * @ORM\Table(name="system_user")
 * @ORM\Entity(repositoryClass="LookAtHotel\SystemBundle\Entity\Repository\SystemUserRepository")
 * @UniqueEntity(fields="username", message="unique_entity", groups={"profile"})
 * @UniqueEntity(fields="email", message="unique_entity", groups={"profile"})
 */
class SystemUser implements AdvancedUserInterface, \Serializable, \Symfony\Component\Security\Core\User\EquatableInterface
{
    
    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"profile"})
     * @Assert\Length(
     *     min=4,
     *     max=50, 
     *     minMessage="min_message",
     *     maxMessage="max_message",
     *     groups={"profile"}
     * )
     * @Assert\Regex(pattern="/^[a-zA-Z]{1}+[a-zA-Z0-9_-]{1,}$/i", match=true, message="username", groups={"profile"})
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"password"})
     * @Assert\Length(
     *     min=8,
     *     max=16, 
     *     minMessage="min_message",
     *     maxMessage="max_message",
     *     groups={"password"}
     * )
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"profile"})
     * @Assert\Length(
     *     min=3,
     *     max=50, 
     *     minMessage="min_message",
     *     maxMessage="max_message",
     *     groups={"profile"}
     * )
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"profile"})
     * @Assert\Length(
     *     min=3,
     *     max=50, 
     *     minMessage="min_message",
     *     maxMessage="max_message",
     *     groups={"profile"}
     * )
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank", groups={"profile"})
     * @Assert\Email(message="email", groups={"profile"})
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime", nullable=false)
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var integer
     *
     * @ORM\Column(name="login_attemps", type="integer", nullable=false)
     */
    private $loginAttemps = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     * @Assert\Choice(choices = {"0", "1"}, message = "choose_one_option", groups={"profile"})
     */
    private $active = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean", nullable=false)
     */
    private $locked = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="locked_until", type="datetime", nullable=true)
     */
    private $lockedUntil;

    /**
     * @var boolean
     *
     * @ORM\Column(name="change_password", type="boolean", nullable=false)
     */
    private $changePassword = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\SystemRole", inversedBy="systemUser")
     * @ORM\JoinTable(name="system_user_role",
     *   joinColumns={
     *     @ORM\JoinColumn(name="system_user_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="system_role_id", referencedColumnName="id")
     *   }
     * )
     * 
     *  @Assert\Count(
     *      min = "1",
     *      max = "10",
     *      minMessage = "You must specify at least one email",
     *      maxMessage = "You cannot specify more than {{ limit }} emails",
     *      groups={"profile"}
     * )
     */
    private $systemRole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->systemRole = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->username;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return SystemUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return SystemUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return SystemUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return SystemUser
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return SystemUser
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return SystemUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return SystemUser
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     * @return SystemUser
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime 
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set loginAttemps
     *
     * @param integer $loginAttemps
     * @return SystemUser
     */
    public function setLoginAttemps($loginAttemps)
    {
        $this->loginAttemps = $loginAttemps;

        return $this;
    }

    /**
     * Get loginAttemps
     *
     * @return integer 
     */
    public function getLoginAttemps()
    {
        return $this->loginAttemps;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return SystemUser
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     * @return SystemUser
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set lockedUntil
     *
     * @param \DateTime $lockedUntil
     * @return SystemUser
     */
    public function setLockedUntil($lockedUntil)
    {
        $this->lockedUntil = $lockedUntil;

        return $this;
    }

    /**
     * Get lockedUntil
     *
     * @return \DateTime 
     */
    public function getLockedUntil()
    {
        return $this->lockedUntil;
    }

    /**
     * Set changePassword
     *
     * @param boolean $changePassword
     * @return SystemUser
     */
    public function setChangePassword($changePassword)
    {
        $this->changePassword = $changePassword;

        return $this;
    }

    /**
     * Get changePassword
     *
     * @return boolean 
     */
    public function getChangePassword()
    {
        return $this->changePassword;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     * @return SystemUser
     */
    public function addSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole[] = $systemRole;

        return $this;
    }

    /**
     * Remove systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     */
    public function removeSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole->removeElement($systemRole);
    }

    /**
     * Get systemRole
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSystemRole()
    {
        return $this->systemRole;
    }
    
    
     /*
     * Métodos abstractos de la clase
     * AdvancedUserInterface
     */
    public function getRoles()
    {
        // Asignar rol por defecto
//        $systemRole = new \LookAtHotel\SystemBundle\Entity\SystemRole();
//        $systemRole->setRole("ROLE_DEFAULT");
//        $systemRole->setLabel("Rol por Defecto");
//        $systemRole->setDescription("Acceso a la plataforma por defecto");
//        $systemRole->setRoleAdmin(false);

        $roles = array();
        foreach ($this->getSystemRole() as $role)
        {
            $roles[] = $role->getRole();
        }
        $roles[] = "ROLE_DEFAULT";//$systemRole;
       
        return $roles;
    }
    
    public function getRolesObj()
    {
        // Asignar rol por defecto
        $systemRole = new \LookAtHotel\SystemBundle\Entity\SystemRole();
        $systemRole->setRole("ROLE_DEFAULT");
        $systemRole->setLabel("Rol por Defecto");
        $systemRole->setDescription("Acceso a la plataforma por defecto");
        $systemRole->setRoleAdmin(false);

        $roles = array();
        foreach ($this->getSystemRole() as $role)
        {
            $roles[] = $role;
        }
        $roles[] = $systemRole;
        
        //return array('ROLE_USER');
        return $roles;
    }

    public function eraseCredentials()
    {
        return false;
    }

//    public function equals(\LookAtHotel\SystemBundle\Entity\SystemUser $user)
//    {
//        return $this->getUsername() == $user->getUsername();
//    }

    public function isEnabled()
    {
        return $this->active == 1;
    }

    public function isAccountNonExpired()
    {
        return $this->active == 1;
    }

    public function isAccountNonLocked()
    {
        $now = new \DateTime();
        if (($this->locked == 1) && ($this->lockedUntil <= $now)) {
            return true;
        } else {
            return $this->locked == 0;
        }
    }

    public function isCredentialsNonExpired()
    {
        return $this->active == 1;
    }
    
    /*
     * Método para obtener los roles
     * desde RoleInterface
     */
    public function getUserRoles()
    {
        return $this->getSystemRole();
    }


    public function getUserLevel()
    {
        $roles = $this->getRolesObj();

        $level = 0;

        foreach ($roles as $role) {
            if ($role->getLevel() > $level) {
                $level = $role->getLevel();
            }
        }

        return 10;//$level;
    }


    public function serialize()
    {
        return serialize($this->getId());
    }

    public function unserialize($data)
    {
        $this->id = unserialize($data);
    }
    
    /* Implementado EquatableInterface */
    public function isEqualTo(\Symfony\Component\Security\Core\User\UserInterface $user)
    {
        if ($this->getId() == $user->getId())
            return true;
        else
            return false;
    }


}
