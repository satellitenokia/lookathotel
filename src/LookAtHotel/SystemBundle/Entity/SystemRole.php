<?php

namespace LookAtHotel\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SystemRole
 *
 * @ORM\Table(name="system_role")
 * @ORM\Entity
 * @UniqueEntity(fields="role", message="unique_entity")
 * @UniqueEntity(fields="label", message="unique_entity")
 */
class SystemRole implements RoleInterface, \Serializable
{
    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50, nullable=false)
     *   @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *     min=5,
     *     max=50, 
     *     minMessage="min_message",
     *     maxMessage="max_message"
     * )
     * @Assert\Regex(pattern="/^ROLE_+[a-zA-Z0-9_-]{1,}+/i", match=true, 
     *              message="El rol de usuario debe empezar con la frase 'ROLE_', contener letras, números y guión bajo.")
     */    
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *     min=3,
     *     max=50, 
     *     minMessage="min_message",
     *     maxMessage="max_message"
     * )
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *     max=255, 
     *     maxMessage="max_message"
     * )
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="role_admin", type="boolean", nullable=false)
     */
    private $roleAdmin;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\SystemUser", mappedBy="systemRole")
     */
    private $systemUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\Menu", inversedBy="systemRole")
     * @ORM\JoinTable(name="system_role_menu",
     *   joinColumns={
     *     @ORM\JoinColumn(name="system_role_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     *   }
     * )
     */
    private $menu;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\Action", inversedBy="systemRole")
     * @ORM\JoinTable(name="system_permission",
     *   joinColumns={
     *     @ORM\JoinColumn(name="system_role_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     *   }
     * )
     */
    private $action;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\Entity", inversedBy="systemRole")
     * @ORM\JoinTable(name="database_permission",
     *   joinColumns={
     *     @ORM\JoinColumn(name="system_role_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="entity_id", referencedColumnName="id")
     *   }
     * )
     */
    private $entity;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->systemUser = new \Doctrine\Common\Collections\ArrayCollection();
        $this->menu = new \Doctrine\Common\Collections\ArrayCollection();
        $this->action = new \Doctrine\Common\Collections\ArrayCollection();
        $this->entity = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return (string)$this->role;
    }
    
    public function getLevel()
    {
        return 10;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return SystemRole
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return SystemRole
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return SystemRole
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set roleAdmin
     *
     * @param boolean $roleAdmin
     * @return SystemRole
     */
    public function setRoleAdmin($roleAdmin)
    {
        $this->roleAdmin = $roleAdmin;

        return $this;
    }

    /**
     * Get roleAdmin
     *
     * @return boolean 
     */
    public function getRoleAdmin()
    {
        return $this->roleAdmin;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add systemUser
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemUser $systemUser
     * @return SystemRole
     */
    public function addSystemUser(\LookAtHotel\SystemBundle\Entity\SystemUser $systemUser)
    {
        $this->systemUser[] = $systemUser;

        return $this;
    }

    /**
     * Remove systemUser
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemUser $systemUser
     */
    public function removeSystemUser(\LookAtHotel\SystemBundle\Entity\SystemUser $systemUser)
    {
        $this->systemUser->removeElement($systemUser);
    }

    /**
     * Get systemUser
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSystemUser()
    {
        return $this->systemUser;
    }

    /**
     * Add menu
     *
     * @param \LookAtHotel\SystemBundle\Entity\Menu $menu
     * @return SystemRole
     */
    public function addMenu(\LookAtHotel\SystemBundle\Entity\Menu $menu)
    {
        $this->menu[] = $menu;

        return $this;
    }

    /**
     * Remove menu
     *
     * @param \LookAtHotel\SystemBundle\Entity\Menu $menu
     */
    public function removeMenu(\LookAtHotel\SystemBundle\Entity\Menu $menu)
    {
        $this->menu->removeElement($menu);
    }

    /**
     * Get menu
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Add action
     *
     * @param \LookAtHotel\SystemBundle\Entity\Action $action
     * @return SystemRole
     */
    public function addAction(\LookAtHotel\SystemBundle\Entity\Action $action)
    {
        $this->action[] = $action;

        return $this;
    }

    /**
     * Remove action
     *
     * @param \LookAtHotel\SystemBundle\Entity\Action $action
     */
    public function removeAction(\LookAtHotel\SystemBundle\Entity\Action $action)
    {
        $this->action->removeElement($action);
    }

    /**
     * Get action
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Add entity
     *
     * @param \LookAtHotel\SystemBundle\Entity\Entity $entity
     * @return SystemRole
     */
    public function addEntity(\LookAtHotel\SystemBundle\Entity\Entity $entity)
    {
        $this->entity[] = $entity;

        return $this;
    }

    /**
     * Remove entity
     *
     * @param \LookAtHotel\SystemBundle\Entity\Entity $entity
     */
    public function removeEntity(\LookAtHotel\SystemBundle\Entity\Entity $entity)
    {
        $this->entity->removeElement($entity);
    }

    /**
     * Get entity
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEntity()
    {
        return $this->entity;
    }
    
    public function serialize()
    {
        return serialize($this->getId());
    }

    public function unserialize($data)
    {
        $this->id = unserialize($data);
    }
}
