<?php

namespace LookAtHotel\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @ORM\Table(name="action", indexes={@ORM\Index(name="fk_action_controller", columns={"controller_id"})})
 * @ORM\Entity
 */
class Action
{
    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255, nullable=false)
     */
    private $action;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \LookAtHotel\SystemBundle\Entity\Controller
     *
     * @ORM\ManyToOne(targetEntity="LookAtHotel\SystemBundle\Entity\Controller")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="controller_id", referencedColumnName="id")
     * })
     */
    private $controller;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\SystemRole", mappedBy="action")
     */
    private $systemRole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->systemRole = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set action
     *
     * @param string $action
     * @return Action
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set controller
     *
     * @param \LookAtHotel\SystemBundle\Entity\Controller $controller
     * @return Action
     */
    public function setController(\LookAtHotel\SystemBundle\Entity\Controller $controller = null)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return \LookAtHotel\SystemBundle\Entity\Controller 
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Add systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     * @return Action
     */
    public function addSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole[] = $systemRole;

        return $this;
    }

    /**
     * Remove systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     */
    public function removeSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole->removeElement($systemRole);
    }

    /**
     * Get systemRole
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSystemRole()
    {
        return $this->systemRole;
    }
}
