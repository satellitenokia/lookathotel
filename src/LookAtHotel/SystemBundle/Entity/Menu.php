<?php

namespace LookAtHotel\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Menu
 *
 * @ORM\Table(name="menu", indexes={@ORM\Index(name="fk_menu_menu", columns={"menu_parent_id"})})
 * @ORM\Entity
 */
class Menu
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *     min=3,
     *     max=50, 
     *     minMessage="min_message",
     *     maxMessage="max_message"
     * )
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *     min=1,
     *     max=255, 
     *     minMessage="min_message",
     *     maxMessage="max_message"
     * )
     */
    private $path = '#';

    /**
     * @var integer
     *
     * @ORM\Column(name="order_item", type="integer", nullable=false)
     */
    private $orderItem = 1;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_external", type="boolean", nullable=false)
     */
    private $isExternal = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="in_new_tab", type="boolean", nullable=false)
     */
    private $inNewTab = false;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Length(
     *     min=3,
     *     max=255, 
     *     minMessage="max_message",
     *     maxMessage="max_message"
     * )
     */
    private $icon = 'no-icon';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \LookAtHotel\SystemBundle\Entity\Menu
     *
     * @ORM\ManyToOne(targetEntity="LookAtHotel\SystemBundle\Entity\Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu_parent_id", referencedColumnName="id")
     * })
     */
    private $menuParent = null;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\SystemRole", mappedBy="menu")
     */
    private $systemRole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->systemRole = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set title
     *
     * @param string $title
     * @return Menu
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Menu
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set orderItem
     *
     * @param integer $orderItem
     * @return Menu
     */
    public function setOrderItem($orderItem)
    {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * Get orderItem
     *
     * @return integer 
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * Set isExternal
     *
     * @param boolean $isExternal
     * @return Menu
     */
    public function setIsExternal($isExternal)
    {
        $this->isExternal = $isExternal;

        return $this;
    }

    /**
     * Get isExternal
     *
     * @return boolean 
     */
    public function getIsExternal()
    {
        return $this->isExternal;
    }

    /**
     * Set inNewTab
     *
     * @param boolean $inNewTab
     * @return Menu
     */
    public function setInNewTab($inNewTab)
    {
        $this->inNewTab = $inNewTab;

        return $this;
    }

    /**
     * Get inNewTab
     *
     * @return boolean 
     */
    public function getInNewTab()
    {
        return $this->inNewTab;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Menu
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set menuParent
     *
     * @param \LookAtHotel\SystemBundle\Entity\Menu $menuParent
     * @return Menu
     */
    public function setMenuParent(\LookAtHotel\SystemBundle\Entity\Menu $menuParent = null)
    {
        $this->menuParent = $menuParent;

        return $this;
    }

    /**
     * Get menuParent
     *
     * @return \LookAtHotel\SystemBundle\Entity\Menu 
     */
    public function getMenuParent()
    {
        return $this->menuParent;
    }

    /**
     * Add systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     * @return Menu
     */
    public function addSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole[] = $systemRole;

        return $this;
    }

    /**
     * Remove systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     */
    public function removeSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole->removeElement($systemRole);
    }

    /**
     * Get systemRole
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSystemRole()
    {
        return $this->systemRole;
    }
}
