<?php

namespace LookAtHotel\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Controller
 *
 * @ORM\Table(name="controller", indexes={@ORM\Index(name="fk_controller_bundle", columns={"bundle_id"})})
 * @ORM\Entity
 */
class Controller
{
    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=255, nullable=false)
     */
    private $controller;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \LookAtHotel\SystemBundle\Entity\Bundle
     *
     * @ORM\ManyToOne(targetEntity="LookAtHotel\SystemBundle\Entity\Bundle")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bundle_id", referencedColumnName="id")
     * })
     */
    private $bundle;



    /**
     * Set controller
     *
     * @param string $controller
     * @return Controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return string 
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bundle
     *
     * @param \LookAtHotel\SystemBundle\Entity\Bundle $bundle
     * @return Controller
     */
    public function setBundle(\LookAtHotel\SystemBundle\Entity\Bundle $bundle = null)
    {
        $this->bundle = $bundle;

        return $this;
    }

    /**
     * Get bundle
     *
     * @return \LookAtHotel\SystemBundle\Entity\Bundle 
     */
    public function getBundle()
    {
        return $this->bundle;
    }
}
