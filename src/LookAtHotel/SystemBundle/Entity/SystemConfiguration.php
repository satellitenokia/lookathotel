<?php

namespace LookAtHotel\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * SystemConfiguration
 *
 * @ORM\Table(name="system_configuration")
 * @ORM\Entity
 */
class SystemConfiguration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * 
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allow_multiple_sessions", type="boolean", nullable=false)
     * @Assert\NotNull(message="not_blank")
     */
    private $allowMultipleSessions = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_user_devices", type="integer", nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Regex(pattern="/^[0-9]+$/", message="no_integer") 
     */
    private $maxUserDevices = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="session_timeout", type="integer", nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Regex(pattern="/^[0-9]+$/", message="no_integer") 
     */
    private $sessionTimeout = '3600';

    /**
     * @var integer
     *
     * @ORM\Column(name="max_idle_time", type="integer", nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Regex(pattern="/^[0-9]+$/", message="no_integer") 
     */
    private $maxIdleTime = '600';

    /**
     * @var integer
     *
     * @ORM\Column(name="failed_attempts", type="integer", nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Regex(pattern="/^[0-9]+$/", message="no_integer") 
     */
    private $failedAttempts = '5';

    /**
     * @var integer
     *
     * @ORM\Column(name="lock_time", type="integer", nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Regex(pattern="/^[0-9]+$/", message="no_integer") 
     */
    private $lockTime = '1800';

    /**
     * @var integer
     *
     * @ORM\Column(name="activation_code_valid", type="integer", nullable=false)
     * @Assert\NotBlank(message="not_blank")
     * @Assert\Regex(pattern="/^[0-9]+$/", message="no_integer") 
     */
    private $activationCodeValid = '120';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Assert\Date(message="no_valid_format")
     * @Assert\NotBlank(message="not_blank")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="user_editor", type="string", length=50, nullable=false)
     * 
     */
    private $userEditor;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set allowMultipleSessions
     *
     * @param boolean $allowMultipleSessions
     * @return SystemConfiguration
     */
    public function setAllowMultipleSessions($allowMultipleSessions)
    {
        $this->allowMultipleSessions = $allowMultipleSessions;

        return $this;
    }

    /**
     * Get allowMultipleSessions
     *
     * @return boolean 
     */
    public function getAllowMultipleSessions()
    {
        return $this->allowMultipleSessions;
    }

    /**
     * Set maxUserDevices
     *
     * @param integer $maxUserDevices
     * @return SystemConfiguration
     */
    public function setMaxUserDevices($maxUserDevices)
    {
        $this->maxUserDevices = $maxUserDevices;

        return $this;
    }

    /**
     * Get maxUserDevices
     *
     * @return integer 
     */
    public function getMaxUserDevices()
    {
        return $this->maxUserDevices;
    }

    /**
     * Set sessionTimeout
     *
     * @param integer $sessionTimeout
     * @return SystemConfiguration
     */
    public function setSessionTimeout($sessionTimeout)
    {
        $this->sessionTimeout = $sessionTimeout;

        return $this;
    }

    /**
     * Get sessionTimeout
     *
     * @return integer 
     */
    public function getSessionTimeout()
    {
        return $this->sessionTimeout;
    }

    /**
     * Set maxIdleTime
     *
     * @param integer $maxIdleTime
     * @return SystemConfiguration
     */
    public function setMaxIdleTime($maxIdleTime)
    {
        $this->maxIdleTime = $maxIdleTime;

        return $this;
    }

    /**
     * Get maxIdleTime
     *
     * @return integer 
     */
    public function getMaxIdleTime()
    {
        return $this->maxIdleTime;
    }

    /**
     * Set failedAttempts
     *
     * @param integer $failedAttempts
     * @return SystemConfiguration
     */
    public function setFailedAttempts($failedAttempts)
    {
        $this->failedAttempts = $failedAttempts;

        return $this;
    }

    /**
     * Get failedAttempts
     *
     * @return integer 
     */
    public function getFailedAttempts()
    {
        return $this->failedAttempts;
    }

    /**
     * Set lockTime
     *
     * @param integer $lockTime
     * @return SystemConfiguration
     */
    public function setLockTime($lockTime)
    {
        $this->lockTime = $lockTime;

        return $this;
    }

    /**
     * Get lockTime
     *
     * @return integer 
     */
    public function getLockTime()
    {
        return $this->lockTime;
    }

    /**
     * Set activationCodeValid
     *
     * @param integer $activationCodeValid
     * @return SystemConfiguration
     */
    public function setActivationCodeValid($activationCodeValid)
    {
        $this->activationCodeValid = $activationCodeValid;

        return $this;
    }

    /**
     * Get activationCodeValid
     *
     * @return integer 
     */
    public function getActivationCodeValid()
    {
        return $this->activationCodeValid;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return SystemConfiguration
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set userEditor
     *
     * @param string $userEditor
     * @return SystemConfiguration
     */
    public function setUserEditor($userEditor)
    {
        $this->userEditor = $userEditor;

        return $this;
    }

    /**
     * Get userEditor
     *
     * @return string 
     */
    public function getUserEditor()
    {
        return $this->userEditor;
    }
}
