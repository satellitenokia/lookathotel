<?php

namespace LookAtHotel\SystemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity
 *
 * @ORM\Table(name="entity")
 * @ORM\Entity
 */
class Entity
{
    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string", length=255, nullable=false)
     */
    private $entity;

    /**
     * @var string
     *
     * @ORM\Column(name="schema_name", type="string", length=255, nullable=false)
     */
    private $schemaName;

    /**
     * @var string
     *
     * @ORM\Column(name="table_name", type="string", length=255, nullable=false)
     */
    private $tableName;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="LookAtHotel\SystemBundle\Entity\SystemRole", mappedBy="entity")
     */
    private $systemRole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->systemRole = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set entity
     *
     * @param string $entity
     * @return Entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string 
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set schemaName
     *
     * @param string $schemaName
     * @return Entity
     */
    public function setSchemaName($schemaName)
    {
        $this->schemaName = $schemaName;

        return $this;
    }

    /**
     * Get schemaName
     *
     * @return string 
     */
    public function getSchemaName()
    {
        return $this->schemaName;
    }

    /**
     * Set tableName
     *
     * @param string $tableName
     * @return Entity
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;

        return $this;
    }

    /**
     * Get tableName
     *
     * @return string 
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     * @return Entity
     */
    public function addSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole[] = $systemRole;

        return $this;
    }

    /**
     * Remove systemRole
     *
     * @param \LookAtHotel\SystemBundle\Entity\SystemRole $systemRole
     */
    public function removeSystemRole(\LookAtHotel\SystemBundle\Entity\SystemRole $systemRole)
    {
        $this->systemRole->removeElement($systemRole);
    }

    /**
     * Get systemRole
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSystemRole()
    {
        return $this->systemRole;
    }
}
