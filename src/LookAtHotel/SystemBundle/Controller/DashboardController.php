<?php

namespace LookAtHotel\SystemBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//Acces control
use Symfony\Component\Security\Core\SecurityContextInterface;
use LookAtHotel\SystemBundle\Interfaces\InitializableControllerInterface;


class DashboardController extends Controller implements InitializableControllerInterface
{
    /* Verificate permission for Action */
    public function initialize(Request $request, SecurityContextInterface $security_context)
    {
        // obtener roles de la sesion
        $roleList = $this->container->get('session')->get('roleList');
        if(!$roleList || !$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))//no esta autenticado
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }

        // Access Control Layer
        $permiso = $this->get('system.acl')->checkPermission($roleList, $this->container);
        if (!$permiso['permiso'] && !$permiso['sesion'])// Si no tiene privilegios
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }
        else if (!$permiso['permiso'] && $permiso['sesion'])
        {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('ok.create'. $permiso['type']));
            $referer = $request->headers->get('referer');
            if(!$referer)
                $referer = $this->generateUrl('dashboard', array('error' => $permiso['type']));
            $redirect = $this->redirect($referer);
            $redirect->send();
        }
    }

    public function indexAction(Request $request)
    {
        // Comprobar si hubo un error
        $error = $request->query->get('error');
        if ($error)
        {
            if ($error == 'session')
            {
                $this->get('session')->getFlashBag()->add('info', $this->get('translator')->trans('error.session'));
            }
            elseif ($error == 'permission')
            {
                $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.permission'));
            }
        }

        return $this->render('SystemBundle:Dashboard:index.html.twig');
    }

}