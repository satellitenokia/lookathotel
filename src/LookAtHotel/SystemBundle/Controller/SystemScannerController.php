<?php

/*
 * SystemScanner
 *
 * Capturar y almacenar la estructura del sistema
 * en la base de datos para su aplicación en el
 * Sistema de Accesos
 */

namespace LookAtHotel\SystemBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use LookAtHotel\SystemBundle\DependencyInjection\SystemScanner;
//Acces control
use Symfony\Component\Security\Core\SecurityContextInterface;
use LookAtHotel\SystemBundle\Interfaces\InitializableControllerInterface;

/**
 * SystemScanner controller.
 */
class SystemScannerController extends Controller implements InitializableControllerInterface
{

    /* Verificate permission for Action */
    public function initialize(Request $request, SecurityContextInterface $security_context)
    {
        // obtener roles de la sesion
        $roleList = $this->container->get('session')->get('roleList');
        
        if(!$roleList || !$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))//no esta autenticado
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }

        // Access Control Layer
        $permiso = $this->get('system.acl')->checkPermission($roleList, $this->container);
        if (!$permiso['permiso'] && !$permiso['sesion'])// Si no tiene privilegios
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }
        else if (!$permiso['permiso'] && $permiso['sesion'])
        {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('ok.create'. $permiso['type']));
            $referer = $request->headers->get('referer');
            if(!$referer)
                $referer = $this->generateUrl('dashboard', array('error' => $permiso['type']));
            $redirect = $this->redirect($referer);
            $redirect->send();
        }
    }


    // Unico método de la clase que completa la base de datos
    // proceso de inserción de registros nuevos y actualización
    public function scanAction()
    {
        $systemScanner = new SystemScanner($this->container);
        $systemScanner->setExecutor('SystemScannerController');

        $result = $systemScanner->scan();


        return $this->render('SystemBundle:SystemScanner:scan.html.twig', array(
                    'log'       => $result['log'],
                    'logEntity' => $result['logEntity'],
        ));
    }

}