<?php

namespace LookAtHotel\SystemBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use LookAtHotel\SystemBundle\Entity\SystemRole;
use LookAtHotel\SystemBundle\Form\SystemRoleType;
use LookAtHotel\SystemBundle\Form\SystemRoleMenuType;
use LookAtHotel\SystemBundle\Form\SystemRoleSystemPermissionType;
use LookAtHotel\SystemBundle\Form\SystemRoleDatabasePermissionType;
//Acces control
use Symfony\Component\Security\Core\SecurityContextInterface;
use LookAtHotel\SystemBundle\Interfaces\InitializableControllerInterface;

/**
 * SystemRole controller.
 *
 */
class SystemRoleController extends Controller implements InitializableControllerInterface
{

    /* Verificate permission for Action */
    public function initialize(Request $request, SecurityContextInterface $security_context)
    {
        // obtener roles de la sesion
        $roleList = $this->container->get('session')->get('roleList');
        
        if(!$roleList || !$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))//no esta autenticado
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }

        // Access Control Layer
        $permiso = $this->get('system.acl')->checkPermission($roleList, $this->container);
        if (!$permiso['permiso'] && !$permiso['sesion'])// Si no tiene privilegios
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }
        else if (!$permiso['permiso'] && $permiso['sesion'])
        {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('ok.create'. $permiso['type']));
            $referer = $request->headers->get('referer');
            if(!$referer)
                $referer = $this->generateUrl('dashboard', array('error' => $permiso['type']));
            $redirect = $this->redirect($referer);
            $redirect->send();
        }
    }


    /**
     * Lists all SystemRole entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
        {
            $entities = $em->getRepository('SystemBundle:SystemRole')->findAll();
        }
        else
        {
            $repository = $em->getRepository('SystemBundle:SystemRole');

            $qb = $repository->createQueryBuilder('r')
                    ->select('r')
                    ->where('r.role <> ?1')
                    ->setParameter(1, 'ROLE_SUPER_ADMIN')
                    ->getQuery();

            $entities = $qb->getResult();
        }

        return $this->render('SystemBundle:SystemRole:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new SystemRole entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SystemRole();
        $form   = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $roleName = $form['role']->getData();

        $em = $this->getDoctrine()->getManager('client');

        if ($form->isValid())
        {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.create')); //
            return $this->redirect($this->generateUrl('role_show', array('id' => $entity->getId())));
        }

        return $this->render('SystemBundle:SystemRole:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a SystemRole entity.
     *
     * @param SystemRole $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SystemRole $entity)
    {
        $form = $this->createForm(new SystemRoleType(), $entity, array(
            'action' => $this->generateUrl('role_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Displays a form to create a new SystemRole entity.
     *
     */
    public function newAction()
    {
        $entity = new SystemRole();
        $form   = $this->createCreateForm($entity);

        return $this->render('SystemBundle:SystemRole:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SystemRole entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SystemBundle:SystemRole:show.html.twig', array(
                    'entity'      => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SystemRole entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        // Verificar nivel del rol
        $token   = $this->container->get('security.context')->getToken();
        $myLevel = $token->getUser()->getUserLevel();

        $newLevel = $entity->getLevel();

        if ($newLevel > $myLevel)
        {
            $this->get('session')->getFlashBag()->add('error', 'No puede editar un rol superior al suyo.');
            return $this->redirect($this->generateUrl('role'));
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('SystemBundle:SystemRole:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a SystemRole entity.
     *
     * @param SystemRole $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SystemRole $entity)
    {
        $form = $this->createForm(new SystemRoleType(), $entity, array(
            'action' => $this->generateUrl('role_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Edits an existing SystemRole entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.update')); //
            return $this->redirect($this->generateUrl('role_edit', array('id' => $id)));
        }

        return $this->render('SystemBundle:SystemRole:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a SystemRole entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em     = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

            if (!$entity)
            {
                $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
                return $this->redirect($this->generateUrl('role'));
            }

            try
            {
                $em->remove($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.delete')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }
        }

        return $this->redirect($this->generateUrl('role'));
    }

    /**
     * Creates a form to delete a SystemRole entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('role_delete', array('id' => $id)))
                        ->setMethod('POST')
                        ->getForm()
        ;
    }

    /**
     * Displays a form to edit the menu for an existing SystemRole entity.
     *
     */
    public function editMenuAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $form = $this->createEditMenuForm($entity);

        return $this->render('SystemBundle:SystemRole:menu.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to edit a SystemRole entity.
     *
     * @param SystemRole $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditMenuForm(SystemRole $entity)
    {
        $form = $this->createForm(new SystemRoleMenuType(), $entity, array(
            'action' => $this->generateUrl('role_menu_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Edits an existing menu for a SystemRole entity.
     *
     */
    public function updateMenuAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $form = $this->createEditMenuForm($entity);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            try
            {
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.update')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }

            return $this->redirect($this->generateUrl('role_menu_edit', array('id' => $id)));
        }

        return $this->render('SystemBundle:SystemRole:menu.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit the menu for an existing SystemRole entity.
     *
     */
    public function editSystemPermissionAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $form = $this->createEditSystemPermissionForm($entity);

        $bundles     = $em->getRepository('SystemBundle:Bundle')->findBy(array(), array('bundle' => 'ASC'));
        $controllers = $em->getRepository('SystemBundle:Controller')->findBy(array(), array('controller' => 'ASC'));
        $actions     = $em->getRepository('SystemBundle:Action')->findBy(array(), array('action' => 'ASC'));

        return $this->render('SystemBundle:SystemRole:system_permission.html.twig', array(
                    'entity'      => $entity,
                    'form'        => $form->createView(),
                    'bundles'     => $bundles,
                    'controllers' => $controllers,
                    'actions'     => $actions,
        ));
    }

    /**
     * Creates a form to edit a SystemRole entity.
     *
     * @param SystemRole $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditSystemPermissionForm(SystemRole $entity)
    {
        $form = $this->createForm(new SystemRoleSystemPermissionType(), $entity, array(
            'action' => $this->generateUrl('role_system_permission_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Edits an existing menu for a SystemRole entity.
     *
     */
    public function updateSystemPermissionAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $form = $this->createEditSystemPermissionForm($entity);
        $form->handleRequest($request);

        $bundles     = $em->getRepository('SystemBundle:Bundle')->findBy(array(), array('bundle' => 'ASC'));
        $controllers = $em->getRepository('SystemBundle:Controller')->findBy(array(), array('controller' => 'ASC'));
        $actions     = $em->getRepository('SystemBundle:Action')->findBy(array(), array('action' => 'ASC'));

        if ($form->isValid())
        {
            try
            {
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.update')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }

            return $this->redirect($this->generateUrl('role_system_permission_edit', array('id' => $id)));
        }

        return $this->render('SystemBundle:SystemRole:system_permission.html.twig', array(
                    'entity'      => $entity,
                    'form'        => $form->createView(),
                    'bundles'     => $bundles,
                    'controllers' => $controllers,
                    'actions'     => $actions,
        ));
    }

    /**
     * Displays a form to edit the menu for an existing SystemRole entity.
     *
     */
    public function editDatabasePermissionAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $form = $this->createEditDatabasePermissionForm($entity);

        return $this->render('SystemBundle:SystemRole:database_permission.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to edit a SystemRole entity.
     *
     * @param SystemRole $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditDatabasePermissionForm(SystemRole $entity)
    {
        $form = $this->createForm(new SystemRoleDatabasePermissionType(), $entity, array(
            'action' => $this->generateUrl('role_database_permission_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Edits an existing menu for a SystemRole entity.
     *
     */
    public function updateDatabasePermissionAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemRole')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('role'));
        }

        $form = $this->createEditDatabasePermissionForm($entity);

        if ($form->isValid())
        {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.update')); //
            return $this->redirect($this->generateUrl('role_database_permission_edit', array('id' => $id)));
        }

        return $this->render('SystemBundle:SystemRole:database_permission.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

}
