<?php

namespace LookAtHotel\SystemBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use LookAtHotel\SystemBundle\Entity\SystemUser;
use LookAtHotel\SystemBundle\Form\SystemUserType;
use LookAtHotel\SystemBundle\Form\SystemUserEditType;
use LookAtHotel\SystemBundle\Form\SystemUserEditPasswordType;
//Acces control
use Symfony\Component\Security\Core\SecurityContextInterface;
use LookAtHotel\SystemBundle\Interfaces\InitializableControllerInterface;

/**
 * SystemUser controller.
 */
class SystemUserController extends Controller implements InitializableControllerInterface
{

    /* Verificate permission for Action */
    public function initialize(Request $request, SecurityContextInterface $security_context)
    {
        // obtener roles de la sesion
        $roleList = $this->container->get('session')->get('roleList');
        
        if(!$roleList || !$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))//no esta autenticado
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }

        // Access Control Layer
        $permiso = $this->get('system.acl')->checkPermission($roleList, $this->container);
        if (!$permiso['permiso'] && !$permiso['sesion'])// Si no tiene privilegios
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }
        else if (!$permiso['permiso'] && $permiso['sesion'])
        {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('error.permission'));
            $referer = $request->headers->get('referer');
            if(!$referer)
                $referer = $this->generateUrl('dashboard', array('error' => $permiso['type']));
            $redirect = $this->redirect($referer);
            $redirect->send();
        }
    }


    /**
     * Lists all SystemUser entities.
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('SystemBundle:SystemUser')->findAll();

        return $this->render('SystemBundle:SystemUser:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new SystemUser entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SystemUser();
        $form   = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            // Obtener contraseña desde el formulario
            $dbPassword = $form['password']->getData();

            // Preparar salt
            $salt = md5(time());
            $entity->setSalt($salt);

            // Codificar contraseña
            $password = $this->get('system.encoder')->encodePassword($dbPassword, $salt);
            $entity->setPassword($password);

            try
            {
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.create')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }
            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        }

        return $this->render('SystemBundle:SystemUser:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a SystemUser entity.
     *
     * @param SystemUser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SystemUser $entity)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
        {
            $userRole = 'ROLE_SUPER_ADMIN';
        }
        else
        {
            $userRole = 'all';
        }
        $form     = $this->createForm(new SystemUserType($userRole), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Displays a form to create a new SystemUser entity.
     *
     */
    public function newAction()
    {
        $entity = new SystemUser();
        $form   = $this->createCreateForm($entity);

        return $this->render('SystemBundle:SystemUser:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SystemUser entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemUser')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('user'));
        }

        $deleteForm = $this->createDeleteForm($id);
       
        return $this->render('SystemBundle:SystemUser:show.html.twig', array(
                    'entity'      => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SystemUser entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemUser')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('user'));
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('SystemBundle:SystemUser:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a SystemUser entity.
     *
     * @param SystemUser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SystemUser $entity)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN'))
        {
            $userRole = 'ROLE_SUPER_ADMIN';
        }
        else
        {
            $userRole = 'all';
        }
        $form = $this->createForm(new SystemUserEditType($userRole), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Edits an existing SystemUser entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemUser')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('user'));
        }

        $editForm = $this->createEditForm($entity);

        // Roles previos a modificación
        $dbUsername      = $entity->getUsername();
        $dbPreviousRoles = $entity->getRoles();
        $dbRoles         = $editForm['systemRole']->getData();

        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.update')); //
            return $this->redirect($this->generateUrl('user_edit', array('id' => $id)));
        }

        return $this->render('SystemBundle:SystemUser:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a SystemUser entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em     = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SystemBundle:SystemUser')->find($id);

            if (!$entity)
            {
                $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
                return $this->redirect($this->generateUrl('user'));
            }

            try
            {
                $em->remove($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.delete')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }
        }

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a SystemUser entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('user_delete', array('id' => $id)))
                        ->setMethod('POST')
                        ->getForm()
        ;
    }

    /**
     * Displays a form to edit the password of an existing SystemUser entity.
     *
     */
    public function editPasswordAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemUser')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('user'));
        }

        $editForm = $this->createEditPasswordForm($entity);

        return $this->render('SystemBundle:SystemUser:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a SystemUser entity.
     *
     * @param SystemUser $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditPasswordForm(SystemUser $entity)
    {
        $form = $this->createForm(new SystemUserEditPasswordType(), $entity, array(
            'action' => $this->generateUrl('user_password_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Edits an existing SystemUser entity.
     *
     */
    public function updatePasswordAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:SystemUser')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('user'));
        }

        $editForm = $this->createEditPasswordForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            // Preparar salt
            $salt = md5(time());

            // Almacenar salt
            $entity->setSalt($salt);

            // Elementos para la base de datos
            $dbPassword = $editForm['password']->getData();

            // Codificar contraseña
            $password = $this->get('system.encoder')->encodePassword($dbPassword, $salt);
            $entity->setPassword($password);

            try
            {
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'Se ha actualizado el registro seleccionado');
                return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());// Mensaje de error en base de datos
            }
        }

        return $this->render('SystemBundle:SystemUser:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Description: Action para cambiar el lenguaje.
     *
     * @param .
     *
     * @return . */
    public function languageAction(Request $request)
    {
        $language = $_POST['language'];
        $request  = $this->getRequest();
        $request->setLocale($language);
        $this->get('session')->set('_locale', $language);
        //redireccionar a donde estaba
        $referer  = $request->headers->get('referer');
        return new \Symfony\Component\HttpFoundation\RedirectResponse($referer);
    }

}
