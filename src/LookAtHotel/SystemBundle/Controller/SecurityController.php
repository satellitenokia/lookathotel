<?php

namespace LookAtHotel\SystemBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class SecurityController extends Controller
{

    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR))
        {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        else
        {
            $error = '';
        }

        $lastUsername = (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);

        if ($error)
        {
            $error = $error->getMessage(); // WARNING! Symfony source code identifies this line as a potential security threat.

            $em = $this->getDoctrine()->getManager();

            // Obtener configuración del sistema
            $entitySystemConfiguration = $em->getRepository('SystemBundle:SystemConfiguration')->findOneBy(
                    array(), array('createdAt' => 'DESC')
            );

            // Registrar inicio de sesión inválido
            $entitySystemUser = $em->getRepository('SystemBundle:SystemUser')->findOneBy(array(
                'username' => $lastUsername
            ));

            if ($entitySystemUser)
            {
                $loginAttemps = $entitySystemUser->getLoginAttemps();
                $loginAttemps++;

                if ($loginAttemps > $entitySystemConfiguration->getFailedAttempts())
                {
                    $lockedUntil = new \DateTime("+" . $entitySystemConfiguration->getLockTime() . " seconds");
                    $entitySystemUser->setLocked(true);
                    $entitySystemUser->setLockedUntil($lockedUntil);
                }
                else
                {
                    $entitySystemUser->setLoginAttemps($loginAttemps);
                }

                try
                {
                    $em->persist($entitySystemUser);
                    $em->flush();
                }
                catch (\Doctrine\DBAL\DBALException $e)
                {
                    die($e->getMessage());
                }
            }
        }

        return $this->render(
                        'SystemBundle:Security:login.html.twig', array(
                    'last_username' => $lastUsername,
                    'error'         => $error,
                        )
        );
    }

    public function loginCheckAction(Request $request)
    {
        return true;
    }

}