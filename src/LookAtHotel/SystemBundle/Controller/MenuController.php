<?php

namespace LookAtHotel\SystemBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use LookAtHotel\SystemBundle\Entity\Menu;
use LookAtHotel\SystemBundle\Form\MenuParentType;
use LookAtHotel\SystemBundle\Form\MenuChildType;
//Acces control
use Symfony\Component\Security\Core\SecurityContextInterface;
use LookAtHotel\SystemBundle\Interfaces\InitializableControllerInterface;

/**
 * Menu controller.
 *
 */
class MenuController extends Controller implements InitializableControllerInterface
{

    /* Verificate permission for Action */
    public function initialize(Request $request, SecurityContextInterface $security_context)
    {
        // obtener roles de la sesion
        $roleList = $this->container->get('session')->get('roleList');
        
        if(!$roleList || !$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY'))//no esta autenticado
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }

        // Access Control Layer
        $permiso = $this->get('system.acl')->checkPermission($roleList, $this->container);
        if (!$permiso['permiso'] && !$permiso['sesion'])// Si no tiene privilegios
        {
            $redirect = $this->redirect($this->generateUrl('_security_login'));
            $redirect->send();
        }
        else if (!$permiso['permiso'] && $permiso['sesion'])
        {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('ok.create'. $permiso['type']));
            $referer = $request->headers->get('referer');
            if(!$referer)
                $referer = $this->generateUrl('dashboard', array('error' => $permiso['type']));
            $redirect = $this->redirect($referer);
            $redirect->send();
        }
    }


    /**
     * Lists all Menu entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SystemBundle:Menu')->findAll();

        return $this->render('SystemBundle:Menu:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Menu entity.
     *
     */
    public function createAction(Request $request, $type)
    {
        $entity = new Menu();
        $form   = $this->createCreateForm($entity, $type);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            // Persistencia
            try
            {
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.create')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('error.action')); //
            }

            return $this->redirect($this->generateUrl('menu_show', array('id' => $entity->getId())));
        }

        return $this->render('SystemBundle:Menu:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                    'type'   => $type,
        ));
    }

    /**
     * Creates a form to create a Menu entity.
     *
     * @param Menu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Menu $entity, $type)
    {
        if ($type == 'parent')
        {
            $formType  = new MenuParentType();
            $formRoute = 'menu_parent_create';
        }
        else
        {
            $formType  = new MenuChildType();
            $formRoute = 'menu_child_create';
        }

        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl($formRoute),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Displays a form to create a new Menu entity.
     *
     */
    public function newAction($type)
    {
        $entity = new Menu();
        $form   = $this->createCreateForm($entity, $type);

        return $this->render('SystemBundle:Menu:new.html.twig', array(
                    'entity' => $entity,
                    'form'   => $form->createView(),
                    'type'   => $type,
        ));
    }

    /**
     * Finds and displays a Menu entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:Menu')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('menu'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SystemBundle:Menu:show.html.twig', array(
                    'entity'      => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Menu entity.
     *
     */
    public function editAction($type, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:Menu')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('menu'));
        }

        $editForm = $this->createEditForm($entity, $type);

        return $this->render('SystemBundle:Menu:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
                    'type'      => $type,
        ));
    }

    /**
     * Creates a form to edit a Menu entity.
     *
     * @param Menu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Menu $entity, $type)
    {
        if ($type == 'parent')
        {
            $formType  = new MenuParentType();
            $formRoute = 'menu_parent_update';
        }
        else
        {
            $formType  = new MenuChildType();
            $formRoute = 'menu_child_update';
        }

        $form = $this->createForm($formType, $entity, array(
            'action' => $this->generateUrl($formRoute, array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'save'));

        return $form;
    }

    /**
     * Edits an existing Menu entity.
     *
     */
    public function updateAction(Request $request, $type, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SystemBundle:Menu')->find($id);

        if (!$entity)
        {
            $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
            return $this->redirect($this->generateUrl('menu'));
        }

        $editForm = $this->createEditForm($entity, $type);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            // Persistencia
            try
            {
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.update')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('error.action')); //
            }

            if ($type == 'parent')
            {
                return $this->redirect($this->generateUrl('menu_parent_edit', array('id' => $id)));
            }
            else
            {
                return $this->redirect($this->generateUrl('menu_child_edit', array('id' => $id)));
            }
        }

        return $this->render('SystemBundle:Menu:edit.html.twig', array(
                    'entity'    => $entity,
                    'edit_form' => $editForm->createView(),
                    'type'      => $type,
        ));
    }

    /**
     * Deletes a Menu entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em     = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SystemBundle:Menu')->find($id);

            if (!$entity)
            {
                $this->get('session')->getFlashBag()->add('warning', $this->get('translator')->trans('error.record.noExist'));
                return $this->redirect($this->generateUrl('menu'));
            }

            try
            {
                $em->remove($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('ok.delete')); //
            }
            catch (\Doctrine\DBAL\DBALException $e)
            {
                // Mensaje de error en base de datos
                $this->get('session')->getFlashBag()->add('error', $e->getMessage());
            }
        }

        return $this->redirect($this->generateUrl('menu'));
    }

    /**
     * Creates a form to delete a Menu entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('menu_delete', array('id' => $id)))
                        ->setMethod('POST')
                        ->getForm()
        ;
    }
    
    /**
     * Reload the menu
     *
     * @param 
     *
     * @return 
     */
    public function reLoadMenuAction(Request $request)
    {
        $session = $this->container->get('session');
        
        $roles = $this->getUser()->getRolesObj();
        // obtener el menu por cada rol
        foreach ($roles as $role)
        {
            // Comprobar que no sea el Rol por defecto
            if ($role->getRole() != 'ROLE_DEFAULT')
            {
                $menuList[] = $role->getMenu();
            }
        }

        $cleanMenu = null;
        // iterar entre los resultados de menu de cada rol
        // usando el mismo id siempre para evitar que existan
        // duplicados a la hora de implementar el menu
        foreach ($menuList as $menu)
        {
            foreach ($menu as $item)
            {
                $cleanMenu[$item->getId()]['id']    = $item->getId();
                $cleanMenu[$item->getId()]['title'] = $item->getTitle();
                $cleanMenu[$item->getId()]['path']  = $item->getPath();
                if ($item->getMenuParent() == null)
                {
                    $cleanMenu[$item->getId()]['parent'] = '0';
                }
                else
                {
                    $cleanMenu[$item->getId()]['parent']     = $item->getMenuParent()->getId();
                }
                $cleanMenu[$item->getId()]['icon']       = $item->getIcon();
                $cleanMenu[$item->getId()]['order']      = $item->getOrderItem();
                $cleanMenu[$item->getId()]['isExternal'] = $item->getIsExternal();
                $cleanMenu[$item->getId()]['inNewTab']   = $item->getInNewTab();
            }
        }

        if ($cleanMenu != null)
        {
            // ordenar el array de menu según order
            uasort($cleanMenu, function ($a, $b) {
                        return strnatcmp($a['order'], $b['order']);
                    });
        }
        // almacenar menu en la sesión
        $session->set('userMenu', $cleanMenu);
        
        //redireccionar
        $referer = $request->headers->get('referer');
        if(!$referer)
            $referer = $this->generateUrl('dashboard');
        $redirect = $this->redirect($referer);
        $redirect->send();
    }

}
